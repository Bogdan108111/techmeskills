package lesson11HomeWork;

import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class EnterNumberAndFindName {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Map<String, Person> persons = new HashMap<>();

        System.out.println("Введите имя");
        String name = sc.next();
        System.out.println("Введите телефон");
        String phone = sc.next();
        while(!name.equals("stop") && !phone.equals("stop")){
            Person person = new Person(name, phone);
            persons.put(phone, person);
            System.out.println("Введите имя");
            name = sc.next();

            if (name.equals("stop")){
                break;
            }
            System.out.println("Введите номер");
            phone = sc.next();
        }

        for (Person element: persons.values()) {
            System.out.println(element.toString());
        }

        System.out.println("Введите номер телефона, а вам программа выведет имя абонента");
        Person person = persons.get(sc.next());
        System.out.println(person.getName());


    }

}
