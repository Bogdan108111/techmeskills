package lesson9HomeWork.exercise2;

public class TestModelCallService2 {
    public static void main(String[] args) {
        String name = "Zafkiel";
        String pre = "Сэр";
        TestModel2 testmodel2 = new TestModel2();
        // Вывожу фиксированное имя, оно не меняется
        System.out.println(testmodel2.getConstName());
        // Меняем значение name в объекте
        System.out.println(testmodel2.setName(name));
        // Выводим имя с префиксом, значения: Достопочтенный Ольгерд, Благородный Кейстут
        testmodel2.displayInfo(pre + " " + name);
        // Создание объекта с другим именем
        TestModel2 testmodel3 = new TestModel2();
        // Вариант 1
        testmodel3.name = "Gabriel";
        // Вариант 2
        System.out.println(testmodel3.setName("Ratziel"));
    }
}
