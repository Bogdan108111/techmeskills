package lesson9HomeWork.exercise3;

import java.util.InputMismatchException;

public class SurName {

    String surname;

    public String surname(String surname) throws InputMismatchException {


        if (surname.contains("1") ||
                surname.contains("2") ||
                surname.contains("3") ||
                surname.contains("4") ||
                surname.contains("5")){
            System.out.println("Данные введены неверно");
            throw new InputMismatchException();
        }

         this.surname = surname;
        return this.surname;
    }

}
