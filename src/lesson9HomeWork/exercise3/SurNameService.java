package lesson9HomeWork.exercise3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SurNameService {
    public static void main(String[] args) {
        SurName objsurname = new SurName();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите фамилию");
        String inputSurname = scanner.next();
        System.out.println("Считал " + inputSurname);
        try{
            objsurname.surname(inputSurname);

        }
        catch(InputMismatchException e) {
            System.out.println("Введите нормальное значение или программа прекратится");
            objsurname.surname(scanner.next());
        }

    }
}
