package lesson9HomeWork.exercise1.B;

public class Rectangle extends Perimetr {
    private static final String NAME = "Rectangle";

    private double storona1;
    private double storona2;
    private double storona3;
    private double storona4;

    public Rectangle(double storona1, double storona2, double storona3, double storona4) {
        this.storona1 = storona1;
        this.storona2 = storona2;
        this.storona3 = storona3;
        this.storona4 = storona4;
    }

    @Override
    public double getperimetr() {
        return storona1 + storona2 + storona3 + storona4;
    }

    @Override
    public String getName() {
        return NAME;
    }

    public double getStorona1() {
        return storona1;
    }

    public void setStorona1(double storona1) {
        this.storona1 = storona1;
    }

    public double getStorona2() {
        return storona1;
    }

    public void setStorona2(double storona2) {
        this.storona1 = storona2;
    }


    public double getStorona3() {
        return storona3;
    }

    public void setStorona3(double storona3) {
        this.storona3 = storona3;
    }

    public double getStorona4() {
        return storona4;
    }

    public void setStorona4(double storona4) {
        this.storona4 = storona4;
    }

}
