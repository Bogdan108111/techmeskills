package lesson9HomeWork.exercise1.B;

public class Circle extends Perimetr {
    private static final String NAME = "Circle";

    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double getperimetr(){
        double perimetr = Math.PI * radius * radius;
        return perimetr;
    }

    @Override
    public String getName() {
        return NAME;
    }

    public double getradius() {
        return radius;
    }

    public void setradius(double radius) {
        this.radius = radius;
    }

}
