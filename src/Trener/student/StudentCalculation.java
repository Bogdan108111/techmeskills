package Trener;

public class StudentCalculation {
    public static void main(String[] args){
     Student[] students = new Student[3];
     for(int i = 0; i < students.length; i++){
         Student student = new Student();
         switch (i) {
             case 0:
                 student.setName("Masha");
                 student.setGroup(1);
                 student.setGrade(i + 3);
                 break;
             case 1:
                 student.setName("Ivan");
                 student.setGroup(2);
                 student.setGrade(i + 3);
                 break;
             default:
                 student.setName("Sasha");
                 student.setGroup(3);
                 student.setGrade(i + 3);
         }
         students[i] = student;
        }
     for( int i = 0; i < students.length; i++){
         System.out.println(students[i].toString());
     }
    }
}
