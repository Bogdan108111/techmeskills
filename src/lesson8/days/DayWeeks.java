package lesson8.days;

public enum DayWeeks {
    MONDAY(1, "Понедельник"), // Объект
    TUESDAY(2, "Вторник"),
    WEDNESDAY(3, "Среда"),
    THURSDAY(4,"Четверг"),
    FRIDAY(5,"Пятница"),
    SATURDAY(6,"Суббота"),
    SUNDAY( 7,"Воскресенье");

    int numberOfDayWeek; // Свойства
    String nameOfDayWeek;


    //Конструктор
    DayWeeks(int inputDayNumbers, String inputNameOfDayWeek){
        this.numberOfDayWeek = inputDayNumbers;
         nameOfDayWeek = inputNameOfDayWeek;
    }

    //Статичный метод к которому обращаться только через класс т.е DayWeeks.
    public static String getValueOfDayWeekByNumber(int parametrFromScanner){
        DayWeeks[] dayWeeks = DayWeeks.values(); // у enum ксть метод value который возвращает объект в массив
        for (int i = 0; i < dayWeeks.length; i++){
            if(dayWeeks[i].numberOfDayWeek == parametrFromScanner ){
                return dayWeeks[i].nameOfDayWeek;

            }
        }
        return  null;

    }

}
