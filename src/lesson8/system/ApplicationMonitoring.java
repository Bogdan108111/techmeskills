package lesson8.system;

public class ApplicationMonitoring {
    public static void main(String[] args) {
        MonitoringSystem genind =  new GeneralIndicatorModule();
         genind.startMonitoring();
        ErrorMonitoringModule errmon = new ErrorMonitoringModule();
        errmon.startMonitoring();
    }
}
