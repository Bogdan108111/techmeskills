package lesson10HomeWork.exercise1;

public class CallService1 {
    public static void main(String[] args) {
        Model1 model1 = new Model1();
        // Вернули строку из Model
        String testValue1 = model1.getString();
        // Присвоили свойству number номер
        model1.setNumber(4);
        // Возвращаем значение number из Model
        int number = model1.getNumber();
        System.out.println(number);

        Model1 model2 = new Model1(8);
        System.out.println(model2.getNumber());
    }
}
