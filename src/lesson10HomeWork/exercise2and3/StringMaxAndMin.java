package lesson10HomeWork.exercise2and3;

import java.util.Scanner;

public class StringMaxAndMin {
    public static void main(String[] args) {
        System.out.println("Введите");
        Scanner scanner = new Scanner (System.in);
        int k = 5;
        String[] arrString = new String[k];
        int[] lengthString = new int[k];
        for (int i = 0; i < k; i++){
            arrString[i] = scanner.next();
            lengthString[i] = arrString[i].length();
        }
        int freeMin = getMinValue(lengthString);
        int freeMax = getMaxValue(lengthString);

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < k; i++ ){
            if (lengthString[i] == freeMin )
                builder.append(arrString[i]).append(" ").append(lengthString[i]);
            if (lengthString[i] == freeMax )
                builder.append(arrString[i]).append(" ").append(lengthString[i]);

        }
        System.out.println(builder);

    }

    private static int getMinValue(int[] arrQwe) {

        int minValue = arrQwe[0];

        for (int i = 0; i < arrQwe.length; i++) {
            if (arrQwe[i] < minValue)
                minValue = arrQwe[i];
        }

        System.out.println();
        System.out.println("Минимальное значение массива: ");
        System.out.println(minValue);

        return minValue;

    }

    private static int getMaxValue(int[] arrQwe) {

        int maxValue = arrQwe[0];

        for (int i = 0; i < arrQwe.length; i++) {
            if (arrQwe[i] > maxValue)
                maxValue = arrQwe[i];
        }

        System.out.println();
        System.out.println("Максимальное значение массива: ");
        System.out.println(maxValue);

        return maxValue;

    }


}

