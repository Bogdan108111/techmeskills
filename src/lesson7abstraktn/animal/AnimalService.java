package lesson7abstraktn.animal;

import java.util.Date;

public class AnimalService {

    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.setAnimalID(1);
        cat.setName("Barsik");
        Date data = new Date();
        cat.setData(data);
        cat.setEyesColor("Green");

        Dog dog = new Dog();
        dog.setWeight(56.23);
        dog.setAnimalID(2);
        dog.setName("Sharik");
        dog.setData(data);

        Tiger tiger = new Tiger();
        tiger.setAnimalID(1);
        tiger.setName("Iggy");
        tiger.setData(data);
        tiger.setEyesColor("Blue");
        tiger.setInteger(6);

        callPrint(cat);
        callPrint(dog);
        callPrint(tiger);

    }

    private static void callPrint (Animal animal){
        animal.printInfo();
        animal.printVoice();
        String voice = animal.getVoice();
        System.out.println(voice);

    }


}
