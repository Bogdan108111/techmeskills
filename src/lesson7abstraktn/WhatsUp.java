package lesson7abstraktn;

public class  WhatsUp implements Messenger {


    @Override
    public void sendMessage() {
        System.out.println("Отправили сообщение в WhatsUp");

    }

    @Override
    public void getMessage() {
        System.out.println("Получили сообщение в WhatsUp");

    }

}
