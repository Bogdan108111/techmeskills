package lesson11.example3;

import java.util.Objects;

public class Person {

    String name;
    String phoneNumber;

    Person(String name, String phoneNumber){
    this.name = name;
    this.phoneNumber = phoneNumber;
    }

    String getName(){
       return this.name;
    }

    String getPhoneNumber(){
        return this.phoneNumber;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
