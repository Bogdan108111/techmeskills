package lesson11;

import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.Map;

public class SampleUseMap {
    public static void main(String[] args) {
        Map<String,Integer> mapByStr = new HashMap<>();
        mapByStr.put("один",1);
        mapByStr.put("два",2);
        mapByStr.put("три",3);
        System.out.println(mapByStr.get("три"));

        Map<Integer, String> mapByInteger = new HashMap<>();
        mapByInteger.put(1,"один");
        mapByInteger.put(2,"два");
        mapByInteger.put(3,"три");
        System.out.println(mapByInteger.get(3));
        
    }
    
    
    
}
