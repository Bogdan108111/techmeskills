package lesson11.example1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class AverageOfList {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputInt = scanner.next();
        List<Integer> list = new ArrayList<>();
        while(!inputInt.equals("stop")) {
//            int valueFromString = Integer.parseInt(inputInt);
            try{
                list.add(Integer.valueOf(inputInt));
            } catch (Exception ex ){
                System.out.println("Вы ввели не корректное значение. Попробуйте еще раз.");
            }

            inputInt = scanner.next();
        }



        int summelement = 0;

        for (Integer element : list ) {
            summelement = summelement + element;

        }

        double resultat = summelement/list.size();
        System.out.println(resultat);

    }
}
