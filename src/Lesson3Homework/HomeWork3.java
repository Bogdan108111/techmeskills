public class HomeWork3 {
public static void main (String[] args) {
      SortMax();
      SortMin();
      Task3();
       Task4();

}

private static void SortMax(){
    int[] arr = new int[]{3, 2, 1, 4, 5, 7};


    for(int i = 0; i < arr.length; i++) {
        for (int j = 0; j < arr.length - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                int tmp = arr[j + 1];
                arr[j + 1] = arr[j];
                arr[j] = tmp;
            }
        }
    }

    for(int a : arr) {
        System.out.print(a + " ");
    }

}

    private static void SortMin(){
        int[] arr = new int[]{3, 2, 1, 4, 5, 7};


        for(int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                //Сравниваем два элемента
                if (arr[j] < arr[j + 1]) {
                    //Перестановка
                    int tmp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = tmp;
                }
            }

        }

        for(int a : arr) {
            System.out.print(a + " ");
        }

    }


    private static void Task4(){
        int[][] arr = new int[][]{{44, 56, 36, 29},{78, 61, 48, 55}};

        int MinArr = arr[0][0];

        for (int i = 0; i < arr.length; i++) {
            int MinValue = arr[i][0];
            for (int j = 1; j < arr[i].length; j++) {
                if (arr[i][j] > MinValue)
                    MinValue = arr[i][j];
            }
            System.out.println("Минимум: " + MinValue);

            if (MinValue > MinArr) {
                MinArr = MinValue;
            }
        }
        System.out.println(MinArr);
    }


     // это на глупую сортировку, не знаю как сделать
    private static void Task3(){
        int[] arr = new int[]{3, 2, 1, 4, 5, 7};

        for(int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                while (i < arr.length){
                    if (arr[j] > arr[j + 1]) {

                        int tmp = arr[j + 1];
                        arr[j + 1] = arr[j];
                        arr[j] = tmp;
                        i = 0;
                    } else {
                        i++;
                    }

                }

            }

        }

        for(int a : arr) {
            System.out.print(a + " ");
        }

    }

}
