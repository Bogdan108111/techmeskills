package lesson8HomeWork.homework2;

public abstract class Shape {
        public abstract double getArea();
        public abstract String getName();
    }

