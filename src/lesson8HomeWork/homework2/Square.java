package lesson8HomeWork.homework2;

public class Square {
    public static void main(String[] args) {
        Shape[] shape = {new Rectangle(6, 10),
                new Circle(10)};

        for(Shape fig : shape)
            System.out.println(fig.getName() + ": area = " + fig.getArea());
    }
    }

