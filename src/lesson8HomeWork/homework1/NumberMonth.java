package lesson8HomeWork.homework1;

public enum NumberMonth {
    January(1,"Январь"),
    February(2, "Февраль"),
    March(3, "Март"),
    April(4,"Апрель"),
    May(5, "Май"),
    June(6,"Июнь"),
    July(7,"Июль"),
    August(8,"Август"),
    September(9,"Сентябрь"),
    October(10,"Октябрь"),
    November(11,"Ноябрь"),
    December(12,"Декабрь");

    int numberOfMonth;
    String nameOfMonth;

    NumberMonth(int inputNumberMonth, String inputNameMonth) {
        numberOfMonth = inputNumberMonth;
        nameOfMonth = inputNameMonth;
    }

    public static String getValueOfNameMonthByNumber(int parametrFromScanner) {
        NumberMonth[] numberMonth = NumberMonth.values();
        for (NumberMonth nameMonth : numberMonth) {
            if (nameMonth.numberOfMonth == parametrFromScanner) {
                return nameMonth.nameOfMonth;
            }
        }
        return null;
    }
}
