package lesson7;

import java.util.Random;

public class Ocenka {
    public static void main(String[] args) {
        String[] name = new String[]{"Саша", "Паша", "Даша", "Юля", "Наташа"};
        Student[] students = new Student[14];
        for (int i = 0; i < students.length; i++) {
            Student student = new Student(name[getRandom(5)], getRandom(3), getRandom(10));

            students[i] = student;
        }


        System.out.println("Вывод отличников");
        for (int i = 0; i < students.length; i++) {
            students[i].getOnlyGoodStudent();

        }
          System.out.println("Вывод всех");
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].toString());
        }


    }
    private static int getRandom(int MaxValue){
        Random random = new Random ();
        return random.nextInt(MaxValue);

    }
}
