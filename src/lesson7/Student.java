package lesson7;

public class Student {
    Student(){

    }
    public Student(String name, int group, int grade){
        this.name = name;
        this.group = group;
        this.grade = grade;
    }

    private String name;
    private int group;
    private int grade;

    public void setGrade(int grade) {
        this.grade = grade;
    }
    public int getGrade(){
        return grade;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setGroup(int group) {
        this.group = group;
    }
    public int getGroup(){
        return group;
    }

    @Override
    public String toString() {
        return "Имя " + name + " Группа " + group + " Оценка " + grade;
    }

   public void getOnlyGoodStudent () {
       if (grade >= 9) {
           System.out.println(toString());
       }
   }

   public void groupThree () {
        if (group == 3) {
            System.out.println(toString());
        }
   }

}
